Vagrant LAMP
============

It' WEB server for Drupal 7 WEB projects with Bootstrapp.

WEB server vagrant file based on http://www.github.com/mattandersen/vagrant-lamp

I just customize Drupal install provision.sh

Requirements
------------
* VirtualBox <http://www.virtualbox.com>
* Vagrant <http://www.vagrantup.com>
* Git <http://git-scm.com/>

Usage
-----

### Startup 
	$ git clone https://bitbucket.org/mcxlan/7-x-vagrant-bs-1-0 .  
	$ vagrant up

That is pretty simple.

### Connecting

#### Apache
The Apache server is available at <http://localhost:8821>

#### MySQL
Externally the MySQL server is available at port 8881, and when running on the VM it is available as a socket or at port 3306 as usual.
Username: root
Password: root

Technical Details
-----------------
* Ubuntu 14.04 64-bit
* Apache 2
* PHP 5.5
* MySQL 5.5
* Drush
* SASS
* Bootstrapp

We are using the base Ubuntu 14.04 box from Vagrant. If you don't already have it downloaded
the Vagrantfile has been configured to do it for you. This only has to be done once
for each account on your host computer.

The web root is located in the project directory at `src/` and you can install your files there

And like any other vagrant file you have SSH access with

	$ vagrant ssh
